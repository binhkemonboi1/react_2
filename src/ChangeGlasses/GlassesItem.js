import React, { Component } from 'react'

export default class GlassesItem extends Component {
    render() {
        let { img, name, price } = this.props;
        return (
            <div className="col-4" onClick={() => {
                this.props.pickGlass(img, name, price);
            }}>
                <div className="card-p3 glass-item-card">
                    <img className="card-img-top" src={img} alt={name} />
                    <div className="card-body">
                        <h4 className="card-title">{name}</h4>
                        <p className="card-text">Price: {price}$</p>
                    </div>
                </div>
            </div>
        )
    }
}
