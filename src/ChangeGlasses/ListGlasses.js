import React, { Component } from 'react'
import GlassItem from './GlassesItem'

export default class
    extends Component {
    renderGlasses = (listGlasses) => {
        return listGlasses.map((glass, index) => {
            return <GlassItem pickGlass={this.props.pickGlass} key={index} img={glass.url} name={glass.name}
                price={glass.price} />
        });
    }
    render() {
        console.log(this.props);
        let { ListGlasses } = this.props;
        return (
            <div className="ListGlasses">
                <div className="row">
                    {this.renderGlasses(ListGlasses)}
                </div>
            </div>
        );
    }
}
